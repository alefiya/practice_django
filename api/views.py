from django.shortcuts import render
# Create your views here.
import json
import os
import time
from datetime import datetime
import user

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse

from rest_framework.decorators import permission_classes
from oauth2_provider.decorators import rw_protected_resource
from oauth2_provider.decorators import protected_resource
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope

@csrf_exempt
@api_view(['GET', 'POST', ])
# @permission_classes([TokenHasReadWriteScope])
def index(request):
    content = {'index': "success"}
    return Response(content)