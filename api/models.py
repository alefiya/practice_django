from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.utils import timezone

class Question(models.Model):
    title = models.CharField(max_length=1000)
    private = models.BooleanField(default=False)
    user = models.ForeignKey(User)
    date_added = models.DateTimeField(default=timezone.now)
    date_modified = models.DateTimeField(default=timezone.now)

class Answer(models.Model):
    body = models.CharField(max_length=9000)
    question = models.ForeignKey(Question)
    user = models.ForeignKey(User)
    date_added = models.DateTimeField(default=timezone.now)
    date_modified = models.DateTimeField(default=timezone.now)

class Tenant(models.Model):
    name = models.CharField(max_length=9000)
    api_key = models.CharField(max_length=100)
    date_added = models.DateTimeField(default=timezone.now)
    date_modified = models.DateTimeField(default=timezone.now) 