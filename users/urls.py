from django.views.static import serve
from . import views
# from django_practice.settings import MEDIA_ROOT
# for oauth
from django.contrib import admin
admin.autodiscover()
from rest_framework import permissions, routers, serializers, viewsets
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope

router = routers.DefaultRouter()
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^', include(router.urls)),# oath url's
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]

urlpatterns += [
    url(r'^assets/(?P<path>.*)$', serve, {'document_root': MEDIA_ROOT}),
]